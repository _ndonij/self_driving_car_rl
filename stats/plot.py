import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.ticker import FormatStrFormatter

number = 8
filename = "test-" + str(number) + ".csv"

df = pd.read_csv(filename)

# # episode length plot
plt.plot(df["episode"], df["episode_len"])
plt.xlabel('episodes')
plt.ylabel('covered distance (steps)')
plt.savefig('plots/steps_test-' + str(number) + '.png')
plt.cla()

# average Q-value plot
plt.plot(df["episode"], df["avg_Q-value"], )
plt.xlabel('episodes')
plt.ylabel('avg Q value')
plt.savefig('plots/Q_test-' + str(number) + '.png')
plt.cla()

# # average reward plot
plt.plot(df["episode"], df["avg_reward"])
plt.xlabel('episodes')
plt.ylabel('avg reward')
plt.savefig('plots/reward_test-' + str(number) + '.png')
