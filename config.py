ENV_NAME = "donkey-generated-roads-v0"
EPISODES = 200
TRAIN = True
HEADLESS = False
STATS_FILE = "./stats/test-8.csv"
MODEL_PATH = "./save_model/"
MODEL_NAME = "model_test-8.h5"
MODEL_TYPE = 2  # 1 = atari | 2 = custom_cnn

ACTION_SPACE = 15
LANE_DETECTION_TYPE = 3  # 1 = bw raw images | 2 = lane detection | 3 = points detection

IMG_ROWS, IMG_COLS = 2, 4
IMG_STACK = 4

THROTTLE_MIN = 0.3
THROTTLE_MAX = 0.8
# Cross track error max - donkey_sim.py
CTE_MAX_ERR = 4.0
# Cross track error limit for penalty - donkey_sim.py
CTE_LIMIT = 3.0
MAX_STEERING_DIFF = 0.3

# These are hyper parameters for the DQN
DISCOUNT = 0.99
LEARNING_RATE = 1e-4
EPSILON_MIN = 0.02
BATCH_SIZE = 64
TRAIN_START = 100
EXPLORE = 10000
MAX_REPLAY = 10000
